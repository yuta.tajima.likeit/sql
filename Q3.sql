﻿CREATE TABLE item_category (category_id int, category_name varchar(30));
ALTER TABLE item_category CONVERT TO CHARACTER SET utf8mb4;
INSERT INTO item_category (category_id,category_name) values (1,'家具');
INSERT INTO item_category (category_id,category_name) values (2,'食品');
INSERT INTO item_category (category_id,category_name) values (3,'本');
